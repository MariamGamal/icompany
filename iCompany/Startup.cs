﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(iCompany.Startup))]
namespace iCompany
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
