﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using iCompany.Models;

namespace iCompany.Controllers
{
    public class ManufactureCountriesController : Controller
    {
        private iCompanyDBEntities db = new iCompanyDBEntities();

        // GET: ManufactureCountries
        public ActionResult Index()
        {
            var manufactureCountries = db.ManufactureCountries.Include(m => m.Product);
            return View(manufactureCountries.ToList());
        }

        // GET: ManufactureCountries/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ManufactureCountry manufactureCountry = db.ManufactureCountries.Find(id);
            if (manufactureCountry == null)
            {
                return HttpNotFound();
            }
            return View(manufactureCountry);
        }

        // GET: ManufactureCountries/Create
        public ActionResult Create()
        {
            ViewBag.ProductID = new SelectList(db.Products, "ID", "ProductName");
            return View();
        }

        // POST: ManufactureCountries/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,CountryName,ProductID")] ManufactureCountry manufactureCountry)
        {
            if (ModelState.IsValid)
            {
                db.ManufactureCountries.Add(manufactureCountry);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ProductID = new SelectList(db.Products, "ID", "ProductName", manufactureCountry.ProductID);
            return View(manufactureCountry);
        }

        // GET: ManufactureCountries/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ManufactureCountry manufactureCountry = db.ManufactureCountries.Find(id);
            if (manufactureCountry == null)
            {
                return HttpNotFound();
            }
            ViewBag.ProductID = new SelectList(db.Products, "ID", "ProductName", manufactureCountry.ProductID);
            return View(manufactureCountry);
        }

        // POST: ManufactureCountries/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,CountryName,ProductID")] ManufactureCountry manufactureCountry)
        {
            if (ModelState.IsValid)
            {
                db.Entry(manufactureCountry).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ProductID = new SelectList(db.Products, "ID", "ProductName", manufactureCountry.ProductID);
            return View(manufactureCountry);
        }

        // GET: ManufactureCountries/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ManufactureCountry manufactureCountry = db.ManufactureCountries.Find(id);
            if (manufactureCountry == null)
            {
                return HttpNotFound();
            }
            return View(manufactureCountry);
        }

        // POST: ManufactureCountries/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ManufactureCountry manufactureCountry = db.ManufactureCountries.Find(id);
            db.ManufactureCountries.Remove(manufactureCountry);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
