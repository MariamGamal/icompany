﻿using iCompany.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace iCompany.Controllers
{
    public class iCompanyController : Controller
    {
     
        public ActionResult Index()
        {
            return View();
        }

        //Get All Products 
        public JsonResult Get_AllProduct()
        {
            using (iCompanyDBEntities Obj = new iCompanyDBEntities())
            {
                List<Product> product = Obj.Products.ToList();
                return Json(product, JsonRequestBehavior.AllowGet);
            }
        }

        //Insert Product 
        public string Insert_Product(Product product)
        {
            if (product != null)
            {
                using (iCompanyDBEntities Obj = new iCompanyDBEntities())
                {
                    Obj.Products.Add(product);
                    Obj.SaveChanges();
                    return "Product Added Successfully";
                }
            }
            else
            {
                return "Product Not Inserted! Please Try Again";
            }
        }

        //Delete Product 
        public string Delete_Product(Product product)
        {
            if (product != null)
            {
                using (iCompanyDBEntities Obj = new iCompanyDBEntities())
                {
                    var pro = Obj.Entry(product);
                    if (pro.State == System.Data.Entity.EntityState.Detached)
                    {
                        Obj.Products.Attach(product);
                        Obj.Products.Remove(product);
                    }
                    Obj.SaveChanges();
                    return "Product Deleted Successfully";
                }
            }
            else
            {
                return "Product Not Deleted! Please Try Again";
            }
        }

        //Update Product 
        public string Update_Product(Product product)
        {
            if (product != null)
            {
                using (iCompanyDBEntities Obj = new iCompanyDBEntities())
                {
                    var pro = Obj.Entry(product);
                    Product proObj = Obj.Products.Where(x => x.ID == product.ID).FirstOrDefault();
                    proObj.ProductName = product.ProductName;
                    proObj.ProductNumber = product.ProductNumber;
                    proObj.PickingAddress = product.PickingAddress;
                    proObj.Weight = product.Weight;
                    proObj.Dimension = product.Dimension;
                    Obj.SaveChanges();
                    return "Product Updated Successfully";
                }
            }
            else
            {
                return "Product Not Updated! Please Try Again";
            }
        }
    }
}