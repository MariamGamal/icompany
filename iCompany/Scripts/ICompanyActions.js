﻿var app = angular.module("myApp", []);
app.controller("myCtrl", function ($scope, $http) {
    debugger;
    $scope.InsertData = function () {
        var Action = document.getElementById("btnSave").getAttribute("value");
        if (Action == "Submit") {
            $scope.Product = {};
            $scope.Product.ProductName = $scope.ProductName;
            $scope.Product.ProductNumber = $scope.ProductNumber;
            $scope.Product.Dimension = $scope.Dimension;
            $scope.Product.Weight = $scope.Weight;
            $scope.PickingAddress = $scope.PickingAddress;
            $http({
                method: "post",
                url: "http://localhost:64468/iCompany/Insert_Product",
                datatype: "json",
                data: JSON.stringify($scope.Product)
            }).then(function (response) {
                alert(response.data);
                $scope.GetAllData();
                $scope.ProductName = "";
                $scope.ProductNumber = "";
                $scope.Dimension = "";
                $scope.Weight = "";
                $scope.PickingAddress = "";
            })
        } else {
            $scope.Product = {};
            $scope.Product.ProductName = $scope.ProductName;
            $scope.Product.ProductNumber = $scope.ProductNumber;
            $scope.Product.Dimension = $scope.Dimension;
            $scope.Product.Weight = $scope.Weight;
            $scope.Product.PickingAddress = $scope.PickingAddress;
            $scope.Product.ID = document.getElementById("ID").value;
            $http({
                method: "post",
                url: "http://localhost:64468/iCompany/Update_Product",
                datatype: "json",
                data: JSON.stringify($scope.Product)
            }).then(function (response) {
                alert(response.data);
                $scope.GetAllData();
                $scope.ProductName = "";
                $scope.ProductNumber = "";
                $scope.Dimension = "";
                $scope.Weight = "";
                $scope.PickingAddress = "";
                document.getElementById("btnSave").setAttribute("value", "Submit");
                document.getElementById("btnSave").style.backgroundColor = "cornflowerblue";
                document.getElementById("spn").innerHTML = "Add New Product";
            })
        }
    }

    $scope.GetAllData = function () {
        $http({
            method: "get",
            url: "http://localhost:64468/iCompany/Get_AllProduct"
        }).then(function (response) {
            $scope.Product = response.data;
        }, function () {
            alert("Error Occur");
        })
    };

    $scope.DeleteProduct = function (Pro) {
        $http({
            method: "post",
            url: "http://localhost:64468/iCompany/Delete_Product",
            datatype: "json",
            data: JSON.stringify(Pro)
        }).then(function (response) {
            alert(response.data);
            $scope.GetAllData();
        })
    };

    $scope.UpdateProduct = function (pro) {
        document.getElementById("ID").value = Pro.ID;
        $scope.ProductName = $scope.ProductName;
        $scope.ProductNumber = $scope.ProductNumber;
        $scope.Dimension = $scope.Dimension;
        $scope.Weight = $scope.Weight;
        $scope.PickingAddress = $scope.PickingAddress;
        document.getElementById("btnSave").setAttribute("value", "Update");
        document.getElementById("btnSave").style.backgroundColor = "Yellow";
        document.getElementById("spn").innerHTML = "Update Product Information";
    }
})